using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api_vct.Models;
using Newtonsoft.Json;

namespace tech_test_payment_api_vct.Controllers
{
    [ApiController]
    [Route("controller")]
    public class VendaController : ControllerBase
    {              

/*  Os inserts de dados dever ser adicionados manualmente no arquivo json na pasta /Controllers;
    O mesmo se faz necessário quando atualizado os status de venda;
*/

        [HttpPost]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Data == DateTime.MinValue)
                return BadRequest(new { Erro = "A data da tarefa não pode ser vazia" });
            if (venda.Item == null)
                return BadRequest(new { Erro = "Necessário pelo menos um item para registrar venda" });
                        
            return Ok(venda);
        }

        [HttpGet("BuscarVendaPorId")]
        public IActionResult Buscarvenda(int id)
        {
            string conteudoArquivo = Venda.ConteudoJsonVendas("Controllers/ListaVendas.json");
            var venda = JsonConvert.DeserializeObject<List<Venda>>(conteudoArquivo);
            var vendaSelecionada = venda.Where(x => x.IdVenda == id);
            if (vendaSelecionada == null)
                return NotFound();
            return Ok(vendaSelecionada);
        }

        [HttpPatch("AtualizarVenda")]
        public IActionResult AtualizarVenda(int id, int status)
        {
            string conteudoArquivo = Venda.ConteudoJsonVendas("Controllers/ListaVendas.json");
            List<Venda> venda = JsonConvert.DeserializeObject<List<Venda>>(conteudoArquivo);
            Venda vendaSelecionada = venda.Single(x => x.IdVenda == id);
            
            if (vendaSelecionada == null)
                return NotFound();

            int statusvenda = Convert.ToInt32(vendaSelecionada.Status);

            switch(statusvenda)
            {
                case 0:
                    if (status != 1 && status != 4)
                        {
                            return BadRequest(new { Erro = "Atualização inválida" });  
                        }
                        else if(status == 1)
                        {
                            vendaSelecionada.Status = EnumStatusVenda.PagamentoAprovado;
                        }
                        else if (status == 4)
                        {
                            vendaSelecionada.Status = EnumStatusVenda.Cancelada;
                        }  
                    break;
                
                case 1:
                    if(status != 2 && status != 4)
                    {
                        return BadRequest(new { Erro = "Atualização inválida" });  
                    }
                    else if(status == 2)
                    {
                        vendaSelecionada.Status = EnumStatusVenda.EnviadoParaTransportadora;
                    }
                    else if (status == 4)
                        {
                            vendaSelecionada.Status = EnumStatusVenda.Cancelada;
                        }
                    break;
                case 2:
                    if(status != 3)
                    {
                        return BadRequest(new { Erro = "Atualização inválida" }); 
                    }
                    else
                    {
                        vendaSelecionada.Status = EnumStatusVenda.Entregue;
                    }
                    break;
                default:
                    return BadRequest(new { Erro = "Nada para atualizar" });
            }
            return Ok(vendaSelecionada);
        }
    }
}