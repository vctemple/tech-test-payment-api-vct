using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api_vct.Models
{
    public class Vendedor
    {
        private int _id;
        private string _cpf;
        private string _nome;
        private string _email;
        private string _telefone;

        public Vendedor(int id, string cpf, string nome, string email, string telefone)
        {
            Id = id;
            CPF = cpf;
            Nome = nome;
            Email = email;
            Telefone = telefone;
        }

        public int Id
        {
            get {return _id; }
            set {_id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
        
        public string CPF
        {
            get { return _cpf; }
            set
            { _cpf = value; }
        }

        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public string Telefone
        {
            get { return _telefone; }
            set { _telefone = value; }
        }
    }
}