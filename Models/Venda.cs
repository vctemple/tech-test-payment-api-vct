using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api_vct.Models
{
    public class Venda
    {
        public int IdVenda { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Produto> Item { get; set; }
        public EnumStatusVenda Status { get; set; }
        public DateTime Data { get; set; }    

        public static string ConteudoJsonVendas(string caminho)
        {
            string conteudo = File.ReadAllText(caminho);
            return conteudo;
        } 
    }

    
}